package data.scripts;

import com.fs.starfarer.api.BaseModPlugin;
import com.fs.starfarer.api.Global;
import com.fs.starfarer.api.PluginPick;
import com.fs.starfarer.api.campaign.CampaignPlugin;
import com.fs.starfarer.api.combat.MissileAIPlugin;
import com.fs.starfarer.api.combat.MissileAPI;
import com.fs.starfarer.api.combat.ShipAPI;
import data.scripts.ai.TADA_pilumAI;
import data.scripts.ai.TADA_spikeAI;
import org.dark.shaders.light.LightData;
import org.dark.shaders.util.ShaderLib;
import org.dark.shaders.util.TextureData;

public class TADA_modPlugin extends BaseModPlugin {

    public static final String spike_ID = "TADA_spike_rod";
    public static final String pilum_ID = "TADA_pilumMissile";
    

    @Override
    public PluginPick<MissileAIPlugin> pickMissileAI(MissileAPI missile, ShipAPI launchingShip) {
        switch (missile.getProjectileSpecId()) {
            case spike_ID:
                return new PluginPick<MissileAIPlugin>(new TADA_spikeAI(missile, launchingShip), CampaignPlugin.PickPriority.MOD_SPECIFIC);
            case pilum_ID:
                return new PluginPick<MissileAIPlugin>(new TADA_pilumAI(missile, launchingShip), CampaignPlugin.PickPriority.MOD_SPECIFIC);
            default:
        }
        return null;
    }
    
//    @Override
//    public PluginPick<AutofireAIPlugin> pickWeaponAutofireAI(WeaponAPI weapon)
//    {
//        switch (weapon.getId())
//        {
//            case dimentionLeft_ID:
//                return new PluginPick<AutofireAIPlugin>(new ART_dimention_beamForceFire(weapon), CampaignPlugin.PickPriority.MOD_SET);
//            case dimentionRight_ID:
//                return new PluginPick<AutofireAIPlugin>(new ART_dimention_beamForceFire(weapon), CampaignPlugin.PickPriority.MOD_SET);
//            default:
//                return null;
//        }
//    }
    
    @Override
    public void onApplicationLoad() throws ClassNotFoundException {
        try {
            Global.getSettings().getScriptClassLoader().loadClass("org.lazywizard.lazylib.ModUtils");
        } catch (ClassNotFoundException ex) {
            String message = System.lineSeparator()
                    + System.lineSeparator() + "LazyLib is required to run at least one of the mods you have installed."
                    + System.lineSeparator() + System.lineSeparator()
                    + "You can download LazyLib at http://fractalsoftworks.com/forum/index.php?topic=5444"
                    + System.lineSeparator();
            throw new ClassNotFoundException(message);
        }
        
        if(Global.getSettings().getModManager().isModEnabled("shaderLib")){
            ShaderLib.init();  
            LightData.readLightDataCSV("data/lights/TADA_light_data.csv"); 
            TextureData.readTextureDataCSV("data/lights/TADA_texture_data.csv"); 
        }
    }
}
